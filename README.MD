Date (Original): 20160918  
Date (Modified): 20180613  

Assume there are at least 5 tutors at the MLC who have to work Mon - Fri each week.  
You are given a file containing their name, pay rate, and how many hours and minutes they worked per day.  
Each workday is entered as Hour:Minutes Here is an example of two worker's timesheet  

Firstname, Lastname, rate, Mon-hours, Tue-hours, Wed-hours, Thur-hours, Fri-hours  
Mary, Joe,11.20,2:00,3:40,1:10,0:40,3:0  
Paul, Sims,12.50,1:45,2:0,1:50,3:30,0:0  

Write a program to:  

  1. read the csv file and print out the information  
  2. develop a struct to hold hour and minute  
  3. :  
	 (a) develop a class to hold each student's information  
	 (b) calculate total hours worked per tutor (rounded to within half hour ie less than 15 min is rounded to 0,  
	     >=15 min is rounded to 30 min, >=45 min is rounded to 1 hour  
	 (c) calculate total pay for each tutor  
  4. display information for each student in list. Information must include, lastname, firstname, total hours worked,  
     total pay for the week  
  5. display total hours worked per day of the week by all tutors(upon request)  
  6. display total paid to all tutors per day of the week (upon request)  
  7. display total hours worked by all tutors for the week (upon request)  
  8. display total paid to all tutors for the week (upon request)  
  
NOTE: (1) You can only use vector or array to hold your tutors.  
You may not use some other exotic program constructs not discussed in class  