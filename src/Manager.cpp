/*
 * Manager.cpp
 *
 *  Created on: Jun 8, 2018
 *      Author: axyd
 */

#include "Manager.h"
	
Manager::Manager () {
	
}

Manager::~Manager () {
	
}

void Manager::calcTtlTime (tutor& t) {
	t.ttlMinutes= t.minutesMon+t.minutesTue+t.minutesWed+t.minutesThu+
				  t.minutesFri;
}

void Manager::calcTtlPay (tutor& t) {
	t.ttlPay= (t.ttlMinutes/60.0)*t.payRate;
}

/* 4. display information for each student in list. Information must include, 
 lastname, firstname, total hours worked, total pay for the week*/
void Manager::showInfo (const vector<tutor> &t) {
	printf("\t## TUTOR INFORMATION ##\n");	
	printf(" %-10s | %-10s | %5s | %3s  |\n", "Last Name", "First Name", "HH:MM", "Pay");
	printf("---------------------------------------\n");
	for(auto it : t) {
		string combo= combineHrMin(it.ttlMinutes);	//minutes to rounded hr:min

		printf(" %-10s | %-10s | %5s | %3d$ |\n", it.lastName.c_str(), it.firstName.c_str(),
			combo.c_str(), it.ttlPay);
	}
}

/* 5. display total hours worked per day of the week by all tutors */
void Manager::showTtlHrPerDay (const vector<tutor>& t) {
	printf("\n\t## TOTAL WORK HOURS PER DAY ##\n");
	printf(" |  MON  |  TUE  |  WED  |  THU  |  FRI  |\n");
	printf(" -----------------------------------------\n");
	
	int mon= 0, tue= 0, wed= 0, thu= 0, fri= 0;
	
	for(auto it : t) {
		mon+= it.minutesMon;
		tue+= it.minutesTue;
		wed+= it.minutesWed;
		thu+= it.minutesThu;
		fri+= it.minutesFri;
	}
	
	cout<<" | "<<combineHrMin(mon)<<" | "<<combineHrMin(tue)<<" | "
		<<combineHrMin(wed)<<" | "<<combineHrMin(thu)<<" | "<<combineHrMin(fri)
		<<" |\n";
}

/* 6. display total paid to all tutors per day of the week */
void Manager::showTtlPayPerDay (const vector<tutor>& t) {
	printf("\n\t## TOTAL PAY PER DAY ##\n");
	printf(" |  MON  |  TUE  |  WED  |  THU  |  FRI  |\n");
	printf(" -----------------------------------------\n");
	
	int mon= 0, tue= 0, wed= 0, thu= 0, fri= 0;
	
	for(auto it : t) {
		mon+= (it.minutesMon/60)*it.payRate;
		tue+= (it.minutesTue/60)*it.payRate;
		wed+= (it.minutesWed/60)*it.payRate;
		thu+= (it.minutesThu/60)*it.payRate;
		fri+= (it.minutesFri/60)*it.payRate;
	}
	
	cout<<" | "<<mon<<" | "<<tue<<" | "<<wed<<" | "<<thu<<" | "<<fri<<" | \n";	
}

/* 7. display total hours worked by all tutors for the week */
void Manager::showTtlHrForWeek (const vector<tutor>& t) {
	cout<<"\n\t## TOTAL HOURS WORKED FOR THE WEEK ##\n";
	
	int total= 0;
	
	for(auto it : t) {
		total+= it.ttlMinutes;
	}
	
	cout<<total/60<<endl;	
}

/* 8. display total paid to all tutors for the week */
void Manager::showTtlPayForWeek (const vector<tutor>& t) {
	cout<<"\n\t## TOTAL PAY FOR THE WEEK ##\n";
	
	int total= 0;
	
	for(auto it : t) {
		total+= it.ttlPay;
	}
	
	cout<<total<<"$"<<endl;
}

/* Stringify mins -> hh:mm 
 /!\ ROUNDS MINUTES UP OR DOWN ACCORDINGLY /!\
*/
string Manager::combineHrMin (const int& minutes) {
	//15m= 0.25, 30m= 0.5, 45m= 0.75
	float hrmins, minsDec;
	int hr;
	string hrStr, minsStr;
	
	//calculate remainder minutes
	hrmins= (minutes/60.0);  //float division
	hr= (minutes/60);		//integer division
	minsDec= hrmins-hr;
	
	//create string representations
	if(minsDec<0.25) minsStr= "00";
	else if(minsDec>=0.25&&minsDec<0.5) minsStr= "30";
	else {
		minsStr= "00";
		hr+= 1;	//rounded to next hour
	}
	
	hrStr= to_string(hr);	
	
	return hrStr+":"+minsStr;	

}
