///////////////////////////////////////////////////////////////////
//Name: Professor Adefemi Sunmonu
//Date: 9/3/2016
//////////////////////////////////////////////////////////////////
using namespace std;
#include <iostream>
#include <fstream> //for reading from file
#include <sstream> //used to store lines from file and to convert integer read from file
#include "utility.h"


class CSVReader {
	public:
		CSVReader(string filepath,char del=',');
		vector<string> getRow(int row) const;//returns a row of the CSV
		string getCell(int row, int col) const;//returns element at row, col
		int getRowsCount() const {
			return Rows.size();
		}; //returns how many rows were read in
		int getColsCount() const {
			return ColsLength;
		};//returns how many cols in the csv
		void printRow(int row) const;//prints the content of a given row

	private:
		vector<string>Rows;//contains the rows of data in the csv
		char del;//delimiter used to separate csv
		int ColsLength=0;//number of columns
};


//implementation of the functions
CSVReader::CSVReader(string filepath,char del) {
	this->del=del;
	//declare the file
	ifstream csvfile;
	csvfile.open(filepath);
	if(csvfile.good()) {
		string line;
		while ( getline(csvfile, line) ) {
			Rows.push_back(line);
			//set the number of columns if necessary
			if(ColsLength==0)
				ColsLength = (split(line,del)).size();
		}
	}
};


///get an entire row as vector
vector<string> CSVReader::getRow(int row) const {
	if(row>=0 && row<Rows.size()) {
		return split(Rows[row],del);
	}
	//return empty list
	vector<string> v;
	return v;
};

///get content in location (row, col)
string CSVReader::getCell(int row, int col) const {
	vector<string> v = getRow(row);
	return v[col];
};

/// print content of a given row
void CSVReader::printRow(int row) const {
	vector<string> vrow= getRow(row);
	cout<<"["<<row<<"] ";
	for(int j=0; j<ColsLength; j++) {
		cout<<vrow[j];
		if(j<ColsLength-1)cout<<",";
	}
	cout<<endl;
}
