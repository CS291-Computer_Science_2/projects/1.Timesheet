#ifndef UTIL_H
#define UTIL_H
///////////////////////////////////////////////////////////////////
//Name: Professor Adefemi Sunmonu
//Date: 9/3/2016
//////////////////////////////////////////////////////////////////
using namespace std;
#include <iostream>
#include <sstream> //used to store lines from file and to convert integer read from file
#include <string>
#include <vector> //used to store the entire content of the csv

////Split a given string using given delimiter. 
 vector<string> split( string s, const char del) 
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != del) buff+=n; else
		if(n == del && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff); 
	
	return v;
};

//converts a given number "1234" to int 1234
int ConvertToInt(const string& s)
{
	int val=0;
    stringstream convertor(s);
    convertor >> val;
	return val;	
};

//converts a given number "123.4" to int 123.4
float ConvertToFloat(const string& s)
{
	float val=0;
    stringstream convertor(s);
    convertor >> val;
	return val;	
};
string its (const int& n)
{
  ostringstream oss;
  oss<< n;
  return oss.str();
};

#endif
