/***************************************************************************
Tutor Timesheet - Jose A Madureira - Fall 2016
#######################################
###Original Coder: Dr. Adefemi Sunmonu#
#######################################
Assume there are at least 5 tutors at the MLC who have to work Mon - Fri each week.
You are given a file containing their name, pay rate, and how many hours and minutes they worked per day. Each workday is entered as Hour:Minutes
Here is an example of two worker's timesheet

Firstname, Lastname, rate, Mon-hours, Tue-hours, Wed-hours, Thur-hours, Fri-hours
Mary, Joe,11.20,2:00,3:40,1:10,0:40,3:0
Paul, Sims,12.50,1:45,2:0,1:50,3:30,0:0

Write a program to
(1) read the csv file and print out the information
(2) develop a struct to hold hour and minute
(3) (a) develop a class to hold each student's information
	(b) calculate total hours worked per tutor (rounded to within half hour ie less than 15 min is rounded to 0, >=15 min is rounded to 30 min, >=45 min is rounded to 1 hour
	(c) calculate total pay for each tutor
(4) display information for each student in list. Information must include, lastname, firstname, total hours worked, total pay for the week
(5) display total hours worked per day of the week by all tutors(upon request)
(6) display total paid to all tutors per day of the week (upon request)
(7) display total hours worked by all tutors for the week (upon request)
(8) display total paid to all tutors for the week (upon request)

NOTE:
(1) You can only use vector or array to hold your tutors. You may not use some other exortic program constructs not discussed in class
(2) If you give this program to someone else to write for you and you are cought, you will be penalized as described in the syllabus (including failing the course and being reported
     appropriate disciplinary body in the College)
-----------------------------------------------------------
Run Timesheet.exe to see how the project begins. Note that Timesheet.exe does not implement all the requirements of this project.
It is just an example!!!
****************************************************************************/
#include "CSVReader.h"
#include "StudentInfo.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>	//remove characters inside strings
using namespace std;

int main() {
//Step 1a - Read the csv file
	CSVReader *rd = new CSVReader("timesheet.csv",',');
	printf("Loaded	 Timesheet  with - Rows:%d, Cols:%d\n",rd->getRowsCount(),rd->getColsCount());
//Step 1b - Print out the content of the csv file
	for(int i=0; i<rd->getRowsCount(); i++) {
		rd->printRow(i);
	}

// 	printf("%s %s's pay rate is increased by $1.20 to $%2.2f",firstname.c_str(),lastname.c_str(),(1.2+payrate));

	int rows = rd->getRowsCount(), cols = rd->getColsCount();	//easy row, col
	char garbage = ' ';
	string tempHolder="";

	vector <StudentInfo> tutors;
	vector <vector<int> > vecHold;	/*##2D Hours##*/
	vector <string> vecSplt;	//split holder [0]=hr, [1]=min
	tutors.clear();

//	for(int i=1; i<rows; i++) {
//		string kill="";
//		vecHold.push_back( vector<int>());	//initialize row(2D)
//		vecSplt.clear();
//		vector <int> vcth;	//HOURS string to int
//		vcth.clear();
//		vector <int> vctm;	//MINUTES string to int
//		vctm.clear();
//		for(int j=3; j<cols; j++) {
//			int cth=0, ctm=0;
////			#Get hour and push to array#
//			tempHolder = rd->getCell(i,j);
//			vecSplt    = split(tempHolder, ':');	//(03:10) to (3 10)
////		/*##Kill split space and put back into array##*/
//			kill = vecSplt[0];
//			kill.erase (remove(kill.begin(), kill.end(), garbage), kill.end());
//			cth = ConvertToInt(kill);
//			ctm = ConvertToInt(vecSplt[1]);
//			vcth.push_back(cth);
//			vctm.push_back(ctm);
//		}


		int hm=vcth[0];
		int ht=vcth[1];
		int hw=vcth[2];
		int hth=vcth[3];
		int hf=vctm[4];
		int mm=vctm[0];
		int mt=vctm[1];
		int mw=vctm[2];
		int mth=vctm[3];
		int mf=vctm[4];
		string fn = rd->getCell(i,0);
		string ln = rd->getCell(i,1);
		float rate = ConvertToFloat(rd->getCell(i,2));
		StudentInfo *tut = new StudentInfo (fn,ln,rate,hm,ht,hw,hth,hf,mm,mt,mw,mth,mf);




		cout<<"\n"<<fn<<" "<<ln<<"\t Weekly pay: "<<tut->getWeeklyPay()<<"\t Weekly Hours: "<<tut->getWeeklyHours();
		/*DOESNT WORK*/
//		tutors.push_back(tut);

//		int mon=vecHold[i-1][0];
//		int tue=vecHold[i-1][1];
//		int wed=vecHold[i-1][2];
//		int thu=vecHold[i-1][3];
//		int fri=vecHold[i-1][4];

	}

	//cout << "\tMon  Tue  Wed  Thu  Fri\n";
	for (int r=0; r<vecHold.size(); r++) {
		cout << "\n\t";
		for (int c=0; c< vecHold[r].size(); c++) {
			cout << " " << vecHold[r][c];
		}
	}


// print student information
	cout<<"\nHere are the information for each tutor in the list\n****************************\n";
	float hrworked=0, totalpay=0;
	for(int i=0; i<rows; i++) {

		//OMITTED - PLEASE FILL THIS IN
	}
	printf("Total Hours Worked in the center this week :%4.2f\nTotal tutors' pay for this week :$%4.2f\n",hrworked,totalpay);


	system("pause");
	return 0;
}
