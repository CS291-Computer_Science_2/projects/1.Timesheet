#include <iostream>
#include <vector>
#include <fstream>	//read file
#include <sstream>	//tokenize string
#include <limits>	//numeric validation
#include "Manager.h"
using namespace std;


void csvReader (string, vector<tutor>&);	//parse file
int toMinutes (string);						//add hours to minutes

int main() {
	string timesheet= "./timesheet.csv";
	vector<tutor> vTutors;
	
	//parse timesheet file
	csvReader(timesheet, vTutors);	
	
	Manager *m= new Manager();
	
	//calculate total time and total pay for the week
	for(auto &it : vTutors) {
		m->calcTtlTime(it);
		m->calcTtlPay(it);
	}
	
	//display general tutor data
	m->showInfo(vTutors);
	
	//allow the user to display specific data
	int option= 0;
	
	do {
		cout<<"\n\t## Tutors Database, enter an option ##\n"
			<<" 1. Total hours worked per day of the week by all tutors\n"
			<<" 2. Total paid to all tutors per day of the week\n"
			<<" 3. Total hours worked by all tutors for the week\n"
			<<" 4. Total paid to all tutors for the week\n"
			<<" 0. Quit\n"
			<<"\n>>choice: ";
		
		while (!(cin>>option)||option>4||option<0) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			
			cout<<"\t/!\\ERROR: invalid choice /!\\ \n"
				<<">>choice: ";
		}
		
		switch (option) {
			case 1:
				system("CLS");
				m->showTtlHrPerDay(vTutors);
				system("PAUSE");
				break;
			case 2:
				system("CLS");
				m->showTtlPayPerDay(vTutors);
				system("PAUSE");
				break;
			case 3:
				system("CLS");
				m->showTtlHrForWeek(vTutors);
				system("PAUSE");
				break;
			case 4:
				system("CLS");
				m->showTtlPayForWeek(vTutors);
				system("PAUSE");
				break;
		}
	}while (option!=0);
	
	delete m;	//cleanup
	return 0;
}

/*Parses tutors timesheet data*/
void csvReader (string fileName, vector<tutor> &tutors) {	
	fstream file;
	file.open(fileName, ios::in);
	
	if(file.fail()) cout<<"\n\tERROR: could not open: "<<fileName<<endl;
	
	string line;
	int lineCtr= 0;
	while (getline(file, line)) {	

		if(lineCtr++) {  //skip header
			tutor t;
			
			//populate new tutor with token data
			istringstream iss(line);
			string token;
			int tknCtr= 0;
			while (getline(iss, token, ',')) {	
				switch (++tknCtr) {
					case 1:
						t.firstName= token;
						break;
					case 2:
						t.lastName= token;
						break;
					case 3:
						t.payRate= stof(token);
						break;
					case 4:
						t.minutesMon= toMinutes(token);
						break;
					case 5:
						t.minutesTue= toMinutes(token);
						break;
					case 6:
						t.minutesWed= toMinutes(token);
						break;
					case 7:
						t.minutesThu= toMinutes(token);
						break;
					case 8:
						t.minutesFri= toMinutes(token);
						break;
				}
			}
			tutors.push_back(t);	//store new tutor
		}
	}
}

/*Convert from hh:mm to only minutes*/
int toMinutes (string hrmin) {
	int hr= 0, min= 0, tknCtr= 0;
	string token;
	
	//tokenize hours and minutes
	istringstream iss(hrmin);
	while (getline(iss, token, ':')) {
		if(tknCtr++==0) hr= stoi(token);
		else min= stoi(token);
	}
	
	return (hr*60)+min;  //convert hours to minutes and sum
}
