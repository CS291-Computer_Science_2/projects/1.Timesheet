/*
 * Manager.h
 *
 *  Created on: Jun 8, 2018
 *      Author: axyd
 */

#ifndef MANAGER_H_
#define MANAGER_H_

#include <iostream>
#include <vector>
using namespace std;

struct tutor {
		//set at creation
		string firstName;
		string lastName;
		float payRate;
		int minutesMon;
		int minutesTue;
		int minutesWed;
		int minutesThu;
		int minutesFri;

		//added post creation
		int ttlMinutes;
		int ttlPay;
};

class Manager {
	public:
		Manager ();
		~Manager ();
		void calcTtlTime (tutor&);
		void calcTtlPay (tutor&);
		void showInfo (const vector<tutor>&);
		void showTtlHrPerDay (const vector<tutor>&);
		void showTtlPayPerDay (const vector<tutor>&);
		void showTtlHrForWeek (const vector<tutor>&);
		void showTtlPayForWeek (const vector<tutor>&);
		
		string combineHrMin (const int&);
};


#endif /* MANAGER_H_ */
